import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';
import ReactDOM from 'react-dom';


class MyReact extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            name : "",
            study : "",
            job : "",
            info:[],
            title: "Simple React CRUD",
            fetch : "All Pushed Data's",
            status :0,
            index : '',
            currentIndex : '',
        }
    }


  /* data insertion start */
    myFun = (e) =>{

        let name = this.refs.name.value;
        let study = this.refs.study.value;
        let job = this.refs.job.value;

        let info = this.state.info;
        let index = this.state.index;

        let status = this.state.status;

        if (status !==1){
          let data  = { name, study, job } ;
          info.push(data);
        }else{
          info[index].name = name;
          info[index].study = study;
          info[index].job = job;
        }

    this.setState({
        info : info,
        name : name,
        study : study,
        job : job,
        status : 0,
        currentIndex : '',
  });
  this.refs.MainForm.reset();
}

/* Data Insertion End here */

/* Data Delete Start From Here */

deleteUser =(i)=>{
    console.log("Index : ",i);

    if (this.state.currentIndex ==i){
        alert("Sir You Can't Delete this Record ");
        return '';
    }

    let info = this.state.info;
    info.splice(i, 1);
    this.setState({
        info: info,
        status:0,
        currentIndex : '',
    });
    this.refs.MainForm.reset();
}

/* Data Delete End Here */

/* Data Edit start */

EditUser = (i) =>{
    let info = this.state.info;
    this.refs.name.value = info[i].name;
    this.refs.study.value = info[i].study;
    this.refs.job.value = info[i].job;

    let currentIndex = this.state.currentIndex;

    this.setState({
        info: info,
        status : 1,
        index : i,
        currentIndex : i,
    });
}

/* Data Edit End */

/* All Data Fetch or Retrive Start */

render(){
    let info = this.state.info;
    let status = this.state.status;
    let currentIndex = this.state.currentIndex;
    return(
        <div className="mainDiv">

            <div className="formDiv">
                <h1 className="headLine"> {this.state.title} </h1>
                <form ref="MainForm">
                    <input type="text" ref="name" className="formField" placeholder="Please input your name" /> <br/>
                    <input type="text" ref="study" className="formField" placeholder="Please input Study Information" /> <br/>
                    <input type="text" ref="job" className="formField" placeholder="Please input your Job Information" /> <br/>
                    <button onClick={(e)=>this.myFun(e)} className="addInfo" type="button"> { status==0 ? "Add Info" : "Update Info"}</button>
                </form>
            </div>


            <div className="rightDiv">
                <h1 className="fetchedData"> {this.state.fetch}     </h1>
                {
                    info.map((myPushedData,i)=>(
                        <div className="curdDiv">
                            <span className="number"> {i+1}</span>
                        <h4 className="username">Name :  { myPushedData.name} </h4>
                        <h4 className="userstudy">Study :  { myPushedData.study} </h4>
                        <h4 className="userjob">Job :  { myPushedData.job} </h4>

                        <button className="deleteUser" onClick={()=>this.deleteUser(i)} > { currentIndex === i ? "Disable" : "Enable" }  </button>
                        <button className="editUser" onClick={()=>this.EditUser(i)}>Edit</button>
                        </div>
                    ))
                }
            </div>
    </div>
)
}
}






/*
 ReactDOM.render(<MyReact/>,document.getElementById('custom'));*/


export default MyReact;
/*

 function App() {
 return (
 <div className="App">
 <header className="App-header">
 <img src={logo} className="App-logo" alt="logo" />
 <p>Hello World React Js</p>
 <p>
 Edit <code>src/App.js</code> and save to reload.
 </p>
 <a
 className="App-link"
 href="https://reactjs.org"
 target="_blank"
 rel="noopener noreferrer"
 >
 Learn React
 </a>

 {/!* Import Class component load start *!/}
 <Person profile/>

 {/!* Import Class Component Load End *!/}
 </header>

 </div>
 );
 }

 */
